package com.ks.comunicaciones;

import com.jcraft.jsch.*;

import java.io.File;
import java.io.FileInputStream;

public class App {
    public static void main(String[] args) {

        try {

            JSch jsch = new JSch();
            Session session = null;

            session = jsch.getSession("jmartine", "10.21.76.147", 22);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword(".J3sus15.");
            session.connect();

            System.out.println("Se conecto para tomar el archivo");

            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;
            sftpChannel.get("/home/jmartine/prueba.txt", "/usr/tandem/DesarrolloKS/prueba/prueba.txt");
            System.out.println("Se tomo el archivo");
            //cerrar conexion
            sftpChannel.exit();
            session.disconnect();
        } catch (Exception ex) {
            System.out.println("Error al tomar archivo: " + ex);
        }

        try{
            JSch jsch = new JSch();
            Session session = null;

            session = jsch.getSession("bancomer", "10.21.76.201", 22);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword(".B4nc0m3r.");
            session.connect();

            System.out.println("Se conecto para subir el archivo");

            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;
            //enviando el archivo
            FileInputStream file = new FileInputStream(new File("/usr/tandem/DesarrolloKS/prueba/prueba.txt"));
            sftpChannel.put(file,"/prueba.txt");
            System.out.println("Se dejo el archivo");
            //cerrar conexion
            sftpChannel.exit();
            session.disconnect();
        }catch (Exception ex){
            System.out.println("Error al subir archivo: " + ex);
        }
    }
}
